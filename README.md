# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Eventorsoft is developped to use with the eventorbot 3D-printer from kickstarter.com

### How do I get set up? ###

* Visualstudio 2012
* Cmake
* Qt5.3
* Qt Plugin for Visual Studio
* Git-Client

### Compile guidelines ###

* First install Visualstudio 2012
* Install Qt5.3
* Install CMake 3.XX
* Install your Git-Client
* Get the repository to your computer
* Take a look at the cmakelists and correct the pathes if neccesarry (If you press configure, CMake will let you know which path is wrong
* Generate the project and choose Visualstudio 2012 (64bit not supported yet)
* Open the generated project with Visual Studio and compile it
### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact