#include "SerialGCodePort.h"

#include <qregexp.h>
#include <qstringlist.h>

SerialGCodePort::SerialGCodePort(QObject *parent):
	QSerialPort(parent)
{
	sendingFileInProgress = false;

	// Connect receive-Signals
	connect(this, SIGNAL(readyRead()), this, SLOT(readData()));
	connect(this, SIGNAL(repeatLineNr()), this, SLOT(sendCommandFromFile()));
	connect(this, SIGNAL(sendNextLine()), this, SLOT(sendCommandFromFile()));
}

SerialGCodePort::SerialGCodePort(const QString &name, QObject * parent):
	QSerialPort(name, parent)
{

}


SerialGCodePort::SerialGCodePort(const QSerialPortInfo & serialPortInfo, QObject * parent):
	QSerialPort(serialPortInfo, parent)
{
}

SerialGCodePort::~SerialGCodePort()
{
	//this->deleteLater();
}

void SerialGCodePort::sendFile(QStringList *file)
{
	gCodeFile = file;
	lineOfFile = 0;
	sendGCommand("M110 N0"+calcCheckSum("M110 N0"));
	emit sendingFileStarted();
	sendingFileInProgress = true;
	sendCommandFromFile();
}

void SerialGCodePort::sendCommandFromFile()
{
	// Send line from File
	if (lineOfFile < gCodeFile->length())
	{
		sendGCommand(gCodeFile->at(lineOfFile));
		emit progressUpdate(lineOfFile, gCodeFile->length());
		// increase linecount
		++lineOfFile;
	}
	else if (lineOfFile == gCodeFile->length())
	{
		// End of File reached
		sendingFileInProgress = false;
		emit sendingFileDone();
		emit progressUpdate(0,0);
		gCodeFile->clear();
	}
	else
	{
		// Error: Invalid File Line Index
		sendingFileInProgress = false;
		emit sendingFileErrorOccured();
	}
}

QString SerialGCodePort::calcCheckSum(QByteArray cmd)
{
	// Checksumme bestimmen
	int cs = 0;
	for(int i = 0; cmd[i] != '*' && cmd[i] != '\0'; i++)
	cs = cs ^ cmd[i];
	cs &= 0xff;  // Defensive programming...
	return "*"+QString::number(cs);
}

void SerialGCodePort::sendGCommand(QString line)
{
	if (line.size() > 0)
	{
		QByteArray cmd = line.toUtf8();
		if (cmd.at(cmd.size()-1) == '\n' || cmd.at(cmd.size()-1) == '\r')
		{
			cmd.remove(cmd.size()-1,1);
		}
		if (QSerialPort::isOpen())
		{
			// G-Command from code
			if (cmd[0] == 'N')
			{
				cmd.append(calcCheckSum(cmd));
				QSerialPort::write(cmd+'\n');
				emit commandSent(cmd, 6);
			}
			// Manual send command
			else if (cmd[0] == 'G')
			{
				QSerialPort::write(cmd+'\n');
				emit commandSent(cmd, 5);
			}
			// M-Command
			else if (cmd[0] == 'M')
			{
				QSerialPort::write(cmd+'\n');
				emit commandSent(cmd, 5);
			}
		}
		//else
		{
			// emit signal to display error
		}
	}
}

void SerialGCodePort::readData()
{
    QByteArray data = SerialGCodePort::readAll(); //reads in one character at a time (or maybe more)
    charBuffer.append(data); 
    if (data.contains("\n")) //read into a structure until newline received.
    {
		if (charBuffer.at(charBuffer.size()-1) == '\n' || charBuffer.at(charBuffer.size()-1) == '\r')
		{
			charBuffer.remove(charBuffer.size()-1,1);
		}
		emitSignal();
    }
}

// This slot is called by 
void SerialGCodePort::emitSignal()
{
	QString reply = QString::fromUtf8(charBuffer);
	charBuffer = "";
	// alles zuordnen und dann die entsprechenden Signale senden
	//QString test = "rs [100] [T:93.2 B:22.9] [C: X:9.2 Y:125.4 Z:3.7 E:1902.5] [dbg]";
	QRegExp input("(rs|ok|!!)\\s?\\[?(\\d+)?\\]?\\s\\[T:(\\d+\\.\\d+)\\s?B:(\\d+\\.\\d+)\\]\\s?\\[C:\\s?X:(\\d+\\.\\d+)\\s?Y:(\\d+\\.\\d+)\\s?Z:(\\d+\\.\\d+)\\s?E:(\\d+\\.\\d+)\\].+\\[(.*)\\]");	
	input.setMinimal(true);
	int i = input.indexIn(reply);
	if (reply.startsWith("ok"))
	{
		QStringList testList = input.capturedTexts();
		float tempT = input.capturedTexts().at(3).toFloat();
		float tempB = input.capturedTexts().at(4).toFloat();
		float X = input.capturedTexts().at(5).toFloat();
		float Y = input.capturedTexts().at(6).toFloat();
		float Z = input.capturedTexts().at(7).toFloat();
		float E = input.capturedTexts().at(8).toFloat();
		QString dbg = input.capturedTexts().at(9);
		emit axisPosChanged(X, Y, Z, tempT, tempB);
		if (sendingFileInProgress)
		{
			emit sendNextLine();
		}
		else
		{
			emit receivedData(reply,1);
		}
	}
	else if (input.capturedTexts().at(1) == "rs")
	{
		if (sendingFileInProgress)
		{
			lineOfFile = input.capturedTexts().at(2).toInt();
			emit receivedData(reply,2);
			emit repeatLineNr();
		}
		else
		{
			emit receivedData(reply,4);
		}
	}
	else if (input.capturedTexts().at(1) == "!!")
	{
		// Display Error and lock different widgets;
		emit receivedData(reply,3);
	}
	else 
	{
		emit receivedData(reply,4);
	}
	//TODO weiteres if f�r endstop antwort
}

void SerialGCodePort::setRelativePosition(int x, int y, int z, int speed)
{
	int x1 = 0, y1 = 0, z1 = 0;
	sendGCommand("G91");
	if (x != 0)
	{
		x1 = x;
	}
	if (y != 0)
	{
		y1 = y;
	}
	if (z != 0)
	{
		z1 = z;
	}
	sendGCommand("G1 x"+QString::number(x1)+" y"+QString::number(y1)+" z"+QString::number(z1)+" f"+QString::number(speed));
}

void SerialGCodePort::setHomePosition(int axis) // x = 1 y = 2 z = 3 -1 = all
{
	switch (axis)
	{
		case 1:
		{
			sendGCommand("G28 X0");
			break;
		}
		case 2:
		{
			sendGCommand("G28 Y0");
			break;
		}
		case 3:
		{
			sendGCommand("G28 Z0");
			break;
		}
		default:
		{
			sendGCommand("G28");
			break;
		}
	}
}

void SerialGCodePort::setTemperature(int degCelsius)
{
	sendGCommand("M109 S"+QString::number(degCelsius)); 
}

void SerialGCodePort::setFanSpeed(int /*fanNr*/, int rpm)
{
	sendGCommand("M106 S"+QString::number(rpm)); 
}