#ifndef SERIALGCODEPORT_H
#define SERIALGCODEPORT_H

#include <qserialport.h>
#include <qserialportinfo.h>

class SerialGCodePort : public QSerialPort
{
	Q_OBJECT

	public:
		SerialGCodePort(QObject* parent = Q_NULLPTR);
		SerialGCodePort(const QString &name, QObject * parent = Q_NULLPTR);
		SerialGCodePort(const QSerialPortInfo & serialPortInfo, QObject * parent = Q_NULLPTR);

		~SerialGCodePort();
		
		void sendGCommand(QString line);

		

		//void getAbsolutePosition(int &x, int &y, int &z);
		//int  getEndStopSwitchesStatus();

		//int  getTemperature();
		//bool isTemperatureReached();

		// Positioning
		void setRelativePosition(int x = 0, int y = 0, int z = 0, int speed = 1000);
		//void setAbsolutePosition(int x = -1, int y = -1, int z = -1, int speed = 1000);
		void setHomePosition(int axis = -1); // x = 1 y = 2 z = 3 -1 = all
		//void setOppositeHomePosition(int axis = -1);
		
		// Temperature regulation
		void setTemperature(int degCelsius);
		void setFanSpeed(int fanNr, int rpm);

		// Other stuff
		//void shutMotorsDown(bool motorsOff);



	public slots:
		// invoked by Send File Button
		void sendFile(QStringList *file);
		void readData();


	private:
		int lineOfFile;
		QStringList *gCodeFile;
		QByteArray charBuffer;
		bool sendingFileInProgress;
		QString calcCheckSum(QByteArray cmd);

	signals:
		void receivedData(QString text, int type);
		void commandSent(QString line, int type);
		void axisPosChanged(int x, int y, int z, int T, int B);
		void sendingFileStarted();
		void sendingFileDone();
		void sendNextLine();
		void repeatLineNr();
		void sendingFileErrorOccured();
		void progressUpdate(int v, int total);


	private slots:
		void emitSignal();
		void sendCommandFromFile();
};

#endif // SERIALGCODEPORT_H