#ifndef EVENTORSOFT_H
#define EVENTORSOFT_H

#include "qmainwindow.h"
#include "ui_eventorsoft.h"

// Widgets
#include "widgets/ConsoleWidget.h"
#include "widgets/StatusMonitorWidget.h"
#include "widgets/ManualWidget.h"
#include "widgets/EditorWidget.h"
#include "widgets/SettingsWidget.h"


// forward declaration
class SerialGCodePort;

class EventorSoft : public QMainWindow
{
	Q_OBJECT

	public:
		EventorSoft(QWidget *parent = 0);
		~EventorSoft();
		SerialGCodePort* getMainPort();

	public slots:
		void setupComPort(QString comPort, int baudrate);


	private:
		Ui::EventorSoftClass ui;
		void createWidgets();
		void readSettings();

		SerialGCodePort *m_serialGCodePort;

		// DockWidgets
		QDockWidget *m_pConsoleDock;
		QDockWidget *m_pEditorDock;
		QDockWidget *m_pManualDock;
		QDockWidget *m_pSettingsDock;
		QDockWidget *m_pStatusMonitorDock;

		// Widgets
		ConsoleWidget *m_pConsoleWidget;
		EditorWidget *m_pEditorWidget;
		ManualWidget *m_pManualWidget;
		SettingsWidget *m_pSettingsWidget;
		StatusMonitorWidget *m_pStatusMonitorWidget;

		

	signals:
		void connectionEstablished(bool enableWidgets);
};

#endif // EVENTORSOFT_H
