#include "eventorsoft.h"

#include <qdockwidget.h>
#include <qsettings.h>
#include <qstandardpaths.h>

#include "classes/SerialGCodePort.h"

EventorSoft::EventorSoft(QWidget *parent): QMainWindow(parent)
{
	ui.setupUi(this);
	createWidgets();

	// Create ComPort Class
	m_serialGCodePort = new SerialGCodePort(this);

	// Read Settings
	readSettings();

	// Connections between Widgets
	connect(this->m_serialGCodePort, SIGNAL(commandSent(QString, int)), m_pConsoleWidget, SLOT(printLine(QString, int)));
	connect(this->m_serialGCodePort, SIGNAL(receivedData(QString, int)), m_pConsoleWidget, SLOT(printLine(QString, int)));
	connect(this->m_pSettingsWidget, SIGNAL(manualSpeedChanged(int , int , int , int , int , int)), m_pManualWidget, SLOT(setStepWidth(int , int , int , int , int , int)));
	connect(this->m_serialGCodePort, SIGNAL(axisPosChanged(int, int, int, int, int)), m_pStatusMonitorWidget, SLOT(valuesChanged(int, int, int, int, int)));
	connect(this->m_pEditorWidget, SIGNAL(startSendingFile(QStringList*)), m_serialGCodePort, SLOT(sendFile(QStringList*)));
	connect(this->m_serialGCodePort, SIGNAL(sendingFileStarted()), m_pConsoleWidget, SLOT(fileSending()));
	connect(this->m_serialGCodePort, SIGNAL(progressUpdate(int, int)), m_pStatusMonitorWidget, SLOT(updateCommandCount(int, int)));
	connect(this->m_serialGCodePort, SIGNAL(sendingFileDone()), m_pConsoleWidget, SLOT(fileDone()));
	connect(this->m_serialGCodePort, SIGNAL(sendingFileErrorOccured()), m_pConsoleWidget, SLOT(fileDone()));

}

EventorSoft::~EventorSoft()
{

}

void EventorSoft::readSettings()
{
	QStringList /*TODO*/ test = QStandardPaths::standardLocations(QStandardPaths::ConfigLocation);
	QSettings settings(QStandardPaths::displayName(QStandardPaths::ConfigLocation), QSettings::IniFormat); 
	settings.beginGroup("mainwindow");
	settings.value("size");
	settings.setValue("fullScreen", false);
    settings.endGroup();
}

SerialGCodePort* EventorSoft::getMainPort()
{
	return m_serialGCodePort;
}



void EventorSoft::createWidgets()
{
	m_pConsoleDock = new QDockWidget(tr("Console"), this);
	m_pConsoleWidget = new ConsoleWidget(m_pConsoleDock);
	m_pConsoleDock->setWidget(m_pConsoleWidget);
	m_pConsoleDock->setFeatures(m_pConsoleDock->NoDockWidgetFeatures);
	this->setCentralWidget(m_pConsoleDock);

	m_pEditorDock = new QDockWidget(tr("Editor"), this);
	m_pEditorWidget = new EditorWidget(m_pEditorDock);
	m_pEditorDock->setFeatures(m_pEditorDock->NoDockWidgetFeatures | QDockWidget::DockWidgetMovable);
	m_pEditorDock->setWidget(m_pEditorWidget);
	this->addDockWidget(Qt::DockWidgetAreas::enum_type::RightDockWidgetArea, m_pEditorDock);

	m_pManualDock = new QDockWidget(tr("Manual"), this);
	m_pManualWidget = new ManualWidget(m_pManualDock);
	m_pManualDock->setFeatures(m_pManualDock->NoDockWidgetFeatures | QDockWidget::DockWidgetMovable);
	m_pManualDock->setWidget(m_pManualWidget);
	this->addDockWidget(Qt::DockWidgetAreas::enum_type::RightDockWidgetArea, m_pManualDock);

	m_pSettingsDock = new QDockWidget(tr("Settings"), this);
	m_pSettingsWidget = new SettingsWidget(m_pSettingsDock);
	m_pSettingsDock->setFeatures(m_pSettingsDock->NoDockWidgetFeatures | QDockWidget::DockWidgetMovable);
	m_pSettingsDock->setWidget(m_pSettingsWidget);
	this->addDockWidget(Qt::DockWidgetAreas::enum_type::LeftDockWidgetArea, m_pSettingsDock);

	m_pStatusMonitorDock = new QDockWidget(tr("Status Monitor"), this);
	m_pStatusMonitorWidget = new StatusMonitorWidget(m_pStatusMonitorDock);
	m_pStatusMonitorDock->setWidget(m_pStatusMonitorWidget);
	m_pStatusMonitorDock->setFeatures(m_pStatusMonitorDock->NoDockWidgetFeatures | QDockWidget::DockWidgetMovable);
	this->addDockWidget(Qt::DockWidgetAreas::enum_type::LeftDockWidgetArea, m_pStatusMonitorDock);

	// These connections are necessary to enable all widgets after connection is established 
	connect(this, SIGNAL(connectionEstablished(bool)), m_pConsoleWidget, SLOT(enableWidget(bool)));
	connect(this, SIGNAL(connectionEstablished(bool)), m_pEditorWidget, SLOT(enableWidget(bool)));
	connect(this, SIGNAL(connectionEstablished(bool)), m_pManualWidget, SLOT(enableWidget(bool)));
	//connect(this, SIGNAL(connectionEstablished(bool)), m_pStatusMonitorWidget, SLOT(enableWidget(bool)));
	connect(this, SIGNAL(connectionEstablished(bool)), m_pSettingsWidget, SLOT(enableTab(bool)));
}

/*Slot invoked by settings widget to set Comport*/
void EventorSoft::setupComPort(QString comPort, int baudrate)
{
	bool baudSet, portSet = false;
	//QList<QSerialPortInfo> ports = QSerialPortInfo
	if (QSerialPortInfo::standardBaudRates().contains(baudrate))
	{
		m_serialGCodePort->setBaudRate(baudrate); //(qint32)baudrate
		baudSet = true;
	}
	if (comPort != "")
	{
		QSerialPortInfo info(comPort);
		m_serialGCodePort->setPort(info);
		portSet = true;
	}
	if (baudSet && portSet)
	{
		m_serialGCodePort->open(QIODevice::OpenMode::enum_type::ReadWrite);
	}
	if (m_serialGCodePort->isOpen())
	{
		emit(connectionEstablished(true));
	}
}
