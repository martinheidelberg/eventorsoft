#include "ConsoleWidget.h"

#include <qwidget.h>
#include <QKeyEvent>
#include <qevent.h>
#include <qlistwidget.h>
#include <eventorsoft.h>
#include <qscrollbar.h>

#include "classes\SerialGCodePort.h"

ConsoleWidget::ConsoleWidget(QWidget *parent): QWidget(parent)
{
	ui.setupUi(this);
	this->setEnabled(false);
	connect(this->parent()->parent(), SIGNAL(commandSent(QString)), this, SLOT(commandSent(QString)));

	m_pCommandListWidget = new QListWidget(this);
	m_pCommandListWidget->setVisible(false);

	ui.lineEdit->installEventFilter(this);
}

ConsoleWidget::~ConsoleWidget()
{
	this->deleteLater();
}

/*slot is used to display anything any widget sends, only called by SerialGCodePort*/
void ConsoleWidget::printLine(QString line, int type)
{ // type: 1:rx:OK, 2:rx:RS, 3:rx:Error, 4:rx:Comment, 5:tx:G-Code, 6:tx:File, 8:tx:Comment
	QString output;
	switch (type)
	{
	case 1:
		{
			output = "<font color=\"black\">"+line+"</font>";
			break;
		}
	case 2:
		{
			output = "<font color=\"orange\">"+line+"</font>";
			break;
		}
	case 3:
		{
			output = "<font color=\"red\">"+line+"</font>";
			break;
		}
	case 4:
		{
			output = "<font color=\"gray\">"+line+"</font>";
			break;
		}
	case 5:
		{
			output = "<font color=\"DeepSkyBlue\">"+line+"</font>";
			break;
		}
	case 6:
		{
			output = "<font color=\"GreenYellow\">"+line+"</font>";
			break;
		}
	case 8:
		{
			output = "<font color=\"Cyan\">"+line+"</font>";
			break;
		}
	default:
		break;
	}
	ui.textBrowser->insertHtml(output+"<br>");
	QScrollBar *sb = ui.textBrowser->verticalScrollBar();
    sb->setValue(sb->maximum());

}

void ConsoleWidget::on_sendButton_clicked()
{
	qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->sendGCommand(ui.lineEdit->text());
	addToCmdHistory(ui.lineEdit->text());
	ui.lineEdit->clear();
}

void ConsoleWidget::addToCmdHistory(QString line)
{
	m_commandHistory.insert(0, line);
	m_commandHistory.removeDuplicates();
	while (m_commandHistory.size() > 10)
	{
		m_commandHistory.removeLast();
	}
}


bool ConsoleWidget::eventFilter(QObject* obj, QEvent *event)
{
    if (obj == ui.lineEdit)
    {
        if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
            if (keyEvent->key() == Qt::Key_Up)
            {
					    
				return true;
            }
            else if(keyEvent->key() == Qt::Key_Down)
            {
				if (m_pCommandListWidget->isVisible())
				{

				}
				else
				{
					m_pCommandListWidget->setVisible(true);
					//m_pCommandListWidget->setCurrentIndex(m_pCommandListWidget->item(0)->);
				}
                return true;
            }
        }
        return false;
    }
    return this->eventFilter(obj, event);
}

void ConsoleWidget::fileSending()
{
	ui.lineEdit->setEnabled(false);
	ui.sendButton->setEnabled(false);
}

void ConsoleWidget::fileDone()
{
	ui.lineEdit->setEnabled(true);
	ui.sendButton->setEnabled(true);
}
/*slot*/
void ConsoleWidget::enableWidget(bool enabled)
{
	this->setEnabled(enabled);
}