#ifndef CONSOLEWIDGET_H
#define CONSOLEWIDGET_H

#include "ui_ConsoleWidget.h"
#include <qevent.h>

// forward declaration
class QListWidget;

class ConsoleWidget : public QWidget
{
	Q_OBJECT

public:
	ConsoleWidget(QWidget *parent = 0);
	~ConsoleWidget();
	

public slots:
	void printLine(QString line, int type);
	void enableWidget(bool enabled);
	void on_sendButton_clicked();
	void fileSending();
	void fileDone();

private:
	Ui::ConsoleWidget ui;
	QStringList m_commandHistory;
	QListWidget *m_pCommandListWidget;

	bool eventFilter(QObject* obj, QEvent *event);
	void addToCmdHistory(QString line);

};

#endif // CONSOLEWIDGET_H