#include "EditorWidget.h"
#include "qwidget.h"
#include "qfiledialog.h"
//#include "qfile.h"
//#include "qdir.h"


EditorWidget::EditorWidget(QWidget *parent): QWidget(parent)
{
	ui.setupUi(this);
	this->setEnabled(false);
}

EditorWidget::~EditorWidget()
{
	this->deleteLater();
}

void EditorWidget::on_loadButton_clicked()
{
	QString gCodePath = QFileDialog::getOpenFileName(this, tr("Open G-Code File"), "C:\\", tr("G-Code Files (*.txt *.*)"));
	if (QFile(gCodePath).exists())
	{
		QFile file(gCodePath);
		file.open(QIODevice::ReadOnly);
		QString gCode = file.readAll();
		highlightText(gCode);
	}
}

void EditorWidget::enableWidget(bool enabled)
{
	this->setEnabled(enabled);
}

void EditorWidget::setLineSend(int lineNr)
{

}

void EditorWidget::highlightText(QString unformattedText)
{
	QStringList lines = unformattedText.split('\n');
	QStringList::iterator it = lines.begin();
	it = lines.begin();
	int lineNumber = 0;
	ui.tableWidget->setColumnCount(1);
	ui.tableWidget->setColumnWidth(0, 100);
	ui.tableWidget->horizontalHeader()->setStretchLastSection(true);
	while (it != lines.end())
	{
		if (it->startsWith('G') || it->startsWith('M')) 
		{
			//lnItem->setTextColor(QColor::darker);
			ui.tableWidget->insertRow(ui.tableWidget->rowCount());
			ui.tableWidget->setItem(lineNumber, 0, new QTableWidgetItem(*it));
			ui.tableWidget->setRowHeight(lineNumber, 21);
			++it;
			++lineNumber;
		}
		else
		{ //it's a comment, remove it
			it = lines.erase(it);
		}
	}
	// tableView->//  extBrowser->setHtml(lines.join("<br>"));
}

void EditorWidget::on_sendButton_clicked()
{
	file.clear();
	QString line = "";
	for (int i = 1; i <= ui.tableWidget->rowCount(); ++i)
	{
		line.clear();
		line.append("N" + QString::number(i) + ' ');
		line.append(ui.tableWidget->item(i-1, 0)->text());
		file.append(line);
	}
	emit startSendingFile(&file);
}