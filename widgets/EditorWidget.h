#ifndef EDITORWIDGET_H
#define EDITORWIDGET_H

#include "ui_EditorWidget.h"
#include "qstringlist.h"

class EditorWidget : public QWidget
{
	Q_OBJECT

	public:
		EditorWidget(QWidget *parent = 0);
		~EditorWidget();

	public slots:
		//void displaySentCmds(QString line);
		//void displayReceivedCmds(QString line);
		void enableWidget(bool enabled);

	private:
		Ui::EditorWidget ui;
		void setLineSend(int lineNr);
		void highlightText(QString unformattedText);

		int m_lineNumber;
		QStringList file;

	private slots:
		void on_loadButton_clicked();
		void on_sendButton_clicked();

	signals:
		void startSendingFile(QStringList *file);

};

#endif // EDITORWIDGET_H