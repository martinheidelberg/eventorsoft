#ifndef MANUALWIDGET_H
#define MANUALWIDGET_H

#include "ui_ManualWidget.h"

//forward declaration
class SerialGCodePort;

class ManualWidget : public QWidget
{
	Q_OBJECT

	public:
		ManualWidget(QWidget *parent = 0);
		~ManualWidget();

	public slots:
		void enableWidget(bool enabled);
		void setStepWidth(int xyf = -1, int xyff = -1, int zf = -1, int zff = -1, int xySpeed = -1, int zSpeed = -1);

	private:
		Ui::ManualWidget ui;
		// members
		int m_xyfStep;
		int m_xyffStep;
		int m_zfStep;
		int m_zffStep;
		int m_xySpeed;
		int m_zSpeed;
		
		SerialGCodePort *mainPort;

	private slots: 

		// X
		void on_ffXIncButton_clicked();
		void on_fXIncButton_clicked();
		void on_ffXDecButton_clicked();
		void on_fXDecButton_clicked();
		void on_xHomeButton_clicked();
		// Y
		void on_ffYIncButton_clicked();
		void on_fYIncButton_clicked();
		void on_ffYDecButton_clicked();
		void on_fYDecButton_clicked();
		void on_yHomeButton_clicked();
		// Z
		void on_ffZIncButton_clicked();
		void on_ffZDecButton_clicked();
		void on_zHomeButton_clicked();

		// Dials
		void on_tempDial_sliderMoved( int value );
		void on_tempDial_sliderReleased();
		void on_fan1Dial_sliderMoved(int value);
		void on_fan1Dial_sliderReleased();


	signals:
};

#endif // MANUALWIDGET_H