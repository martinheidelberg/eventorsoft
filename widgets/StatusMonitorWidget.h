#ifndef STATUSMONITORWIDGET_H
#define STATUSMONITORWIDGET_H

#include "ui_StatusMonitorWidget.h"

class StatusMonitorWidget : public QWidget
{
	Q_OBJECT

public:
	StatusMonitorWidget(QWidget *parent = 0);
	~StatusMonitorWidget();

public slots:
	void valuesChanged(int x = -1, int y = -1, int z = -1, int T = -1, int B = -1);
	void endStopsChanged(bool x, bool y, bool z);
	void updateCommandCount(int v, int total);

private:
	Ui::StatusMonitorWidget ui;
};

#endif // STATUSMONITORWIDGET_H