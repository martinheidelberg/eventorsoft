#include "StatusMonitorWidget.h"
#include "qwidget.h"

StatusMonitorWidget::StatusMonitorWidget(QWidget *parent): QWidget(parent)
{
	ui.setupUi(this);
}

StatusMonitorWidget::~StatusMonitorWidget()
{
	this->deleteLater();
}

void StatusMonitorWidget::valuesChanged(int x, int y, int z, int T, int B)
{
	if (x != -1)
		ui.progressBar->setValue(x);
	if (y != -1)
		ui.progressBar_2->setValue(y);
	if (z != -1)
		ui.progressBar_3->setValue(z);
	if (T > 0)
		ui.lcdNumber->display(T);
	if (B > 0)
		ui.lcdNumber_2->display(B);
}

void StatusMonitorWidget::endStopsChanged(bool x, bool y, bool z)
{
	ui.checkBox->setChecked(x);
	ui.checkBox->setChecked(y);
	ui.checkBox->setChecked(z);
}

void StatusMonitorWidget::updateCommandCount(int v, int total)
{
	if (total == 0)
	{
		ui.progressBar_4->setValue(0);
	}
	else
	{
		ui.progressBar_4->setMaximum(total);
		ui.progressBar_4->setValue(v);
	}
	ui.label_6->setText("Commands send: "+QString::number(v)+"/"+QString::number(total));
}