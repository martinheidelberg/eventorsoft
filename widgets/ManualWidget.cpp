#include "ManualWidget.h"
#include "qwidget.h"
#include "classes/SerialGCodePort.h"
#include "eventorsoft.h"

// Other Classes to have access to gCodeSerialPort

//#include "qdockwidget.h"



ManualWidget::ManualWidget(QWidget *parent): 
	QWidget(parent)
{
	ui.setupUi(this);
	this->setEnabled(false);

	ui.lcdTemp->setPalette(Qt::red);
	ui.lcdNumber->setPalette(Qt::red);

	//mainPort = static_cast<SerialGCodePort*>(static_cast<EventorSoft*>(this->parent()->parent())->getMainPort());
}

ManualWidget::~ManualWidget()
{
	this->deleteLater();
}

void ManualWidget::enableWidget(bool enabled)
{
	this->setEnabled(enabled);
}

void ManualWidget::setStepWidth(int xyf, int xyff, int zf, int zff, int xySpeed, int zSpeed)
{
	if (xyf > 0)
		m_xyfStep  = xyf;
	if (xyff > 0)
		m_xyffStep = xyff;
	if (zf > 0)
		m_zfStep   = zf;
	if (zff > 0)
		m_zffStep  = zff;
	if (xySpeed > 0)
		m_xySpeed  = xySpeed;
	if (zSpeed > 0)
		m_zSpeed   = zSpeed;
}

// X
void ManualWidget::on_ffXIncButton_clicked()
{
	qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->setRelativePosition(m_xyffStep, 0, 0, m_xySpeed);
}

void ManualWidget::on_fXIncButton_clicked()
{
	qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->setRelativePosition(m_xyfStep, 0, 0, m_xySpeed);
}

void ManualWidget::on_ffXDecButton_clicked()
{
	qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->setRelativePosition(-m_xyffStep, 0, 0, m_xySpeed);
}

void ManualWidget::on_fXDecButton_clicked()
{
	qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->setRelativePosition(-m_xyfStep, 0, 0, m_xySpeed);
}

void ManualWidget::on_xHomeButton_clicked()
{
	qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->setHomePosition(1);
}

// Y
void ManualWidget::on_ffYIncButton_clicked()
{
	qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->setRelativePosition(0, m_xyffStep, 0, m_xySpeed);
}

void ManualWidget::on_fYIncButton_clicked()
{
	qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->setRelativePosition(0, m_xyfStep, 0, m_xySpeed);
}

void ManualWidget::on_ffYDecButton_clicked()
{
	qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->setRelativePosition(0, -m_xyffStep, 0, m_xySpeed);
}

void ManualWidget::on_fYDecButton_clicked()
{
	qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->setRelativePosition(0, -m_xyfStep, 0, m_xySpeed);
}

void ManualWidget::on_yHomeButton_clicked()
{
	qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->setHomePosition(2);
}

// Z
void ManualWidget::on_ffZIncButton_clicked()
{
	qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->setRelativePosition(0, 0, m_zffStep, m_zSpeed);
}

void ManualWidget::on_ffZDecButton_clicked()
{
	qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->setRelativePosition(0, 0, -m_zffStep, m_zSpeed);
}

void ManualWidget::on_zHomeButton_clicked()
{
	qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->setHomePosition(3);
}

// Dialer
void ManualWidget::on_tempDial_sliderMoved(int value)
{
	ui.lcdTemp->display(value);
}

void ManualWidget::on_tempDial_sliderReleased()
{
	qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->setTemperature(ui.lcdTemp->intValue());
}

void ManualWidget::on_fan1Dial_sliderMoved(int value)
{
	ui.lcdNumber->display(value);
}

void ManualWidget::on_fan1Dial_sliderReleased()
{
	qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->setFanSpeed(0, ui.lcdNumber->intValue());
}