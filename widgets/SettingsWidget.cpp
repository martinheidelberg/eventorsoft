#include "SettingsWidget.h"

#include <qwidget.h>
#include <QtSerialPort\qserialportinfo.h>

#include "eventorsoft.h"
#include "classes/SerialGCodePort.h"
//#include <QtSerialPort\qserialport.h>


SettingsWidget::SettingsWidget(QWidget *parent): QWidget(parent)
{
	ui.setupUi(this);

	ui.tab_2->setEnabled(false);
	// Setup GUI 
	QStringList labels;
	QList<int> values;
	labels << "Step width XY f" << "Step width XY ff" << "Step width Z f" << "Step width Z ff" << "Speed XY" << "Speed Z";
	
	//TODO Werte aus ini laden
	values << 5 << 10 << 5 << 10 << 1000 << 500;
	
	int row = 0;
	enum MyItemType { INT = 1 };
	foreach(const QString &label, labels)
	{
		ui.settingsTable->insertRow(ui.settingsTable->rowCount());
		QTableWidgetItem *item1 = new QTableWidgetItem(label);
		QTableWidgetItem *item2 = new QTableWidgetItem(QString::number(values[row]), MyItemType::INT);
		ui.settingsTable->setItem(row, 0, item1);
		ui.settingsTable->setItem(row, 1, item2);
		item1->setFlags(Qt::ItemFlags::enum_type::NoItemFlags);
		++row;
	}

	// Connectbutton clicked
	connect(this, SIGNAL(connectClicked(QString, int)), this->parent()->parent(), SLOT(setupComPort(QString, int)));
	// RefreshButton clicked
	connect(ui.refreshButton, SIGNAL(clicked()), this, SLOT(refreshPortProperties()));

	// Fill Boxes with available ports
	refreshPortProperties();
}

SettingsWidget::~SettingsWidget()
{
	this->deleteLater();
}

/*Slot invoked by applyButton_clicked*/
void SettingsWidget::on_connectButton_clicked()
{
	QString baudS = ui.baudCombo->currentText();
	int baud = baudS.toInt();
	ui.tabWidget->setCurrentIndex(1);
	// even the command to set the unit is not received by the machine, it still sets the stepwidth
	ui.applyButton->click();
	emit connectClicked(ui.portCombo->itemText(ui.portCombo->currentIndex()), baud);
}

void SettingsWidget::refreshPortProperties()
{
	//QSerialPortInfo::availablePorts();
	foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
	{
		ui.portCombo->addItem(info.portName());
	}

	// Fill up the Combobox with the standard Baudrates
	foreach(const int &info, QSerialPortInfo::standardBaudRates())
	{
		ui.baudCombo->addItem(QString::number(info));
	}
}

void SettingsWidget::enableWidget(bool enabled)
{
	this->setEnabled(enabled);
}

void SettingsWidget::on_applyButton_clicked()
{
	// Set Speed and Step settings for ManualWidget
	ui.settingsTable->rowCount();
	emit manualSpeedChanged(ui.settingsTable->item(0,1)->text().toInt(), 
							ui.settingsTable->item(1,1)->text().toInt(), 
							ui.settingsTable->item(2,1)->text().toInt(),  
							ui.settingsTable->item(3,1)->text().toInt(), 
							ui.settingsTable->item(4,1)->text().toInt(),
							ui.settingsTable->item(5,1)->text().toInt()); 

	// Set Units
	if (ui.radioButton->isChecked()) //mm
		qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->sendGCommand("G21");
	else if (ui.radioButton_2->isChecked()) //mm
		qobject_cast<SerialGCodePort*>(qobject_cast<EventorSoft*>(this->parent()->parent())->getMainPort())->sendGCommand("G20");

}

void SettingsWidget::enableTab(bool enabled)
{
	ui.tab_2->setEnabled(enabled);
}
