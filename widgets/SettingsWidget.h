#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include "ui_SettingsWidget.h"

class SettingsWidget : public QWidget
{
	Q_OBJECT

	public:
		SettingsWidget(QWidget *parent = 0);
		~SettingsWidget();
	public slots:
		void enableWidget(bool enabled);
		void enableTab(bool enabled);

	signals:
		void connectClicked(QString comPort, int baudrate);
		void manualSpeedChanged(int xyf, int xyff, int zf, int zff, int xySpeed, int zSpeed);

	private:
		Ui::SettingsWidget ui;

	private slots:
		void on_connectButton_clicked();
		void on_applyButton_clicked();
		void refreshPortProperties();
};

#endif // SETTINGSWIDGET_H